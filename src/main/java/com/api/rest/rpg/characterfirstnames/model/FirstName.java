package com.api.rest.rpg.characterfirstnames.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "first_names")
@EntityListeners(AuditingEntityListener.class)
public class FirstName implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "first_name", nullable = false)
	private String firstName;
	
	@Column(name = "description", nullable = true)
	private String description;
	
	@Column(name = "female", nullable = true)
	private Boolean female;
	
	@Column(name = "male", nullable = true)
	private Boolean male;

	public FirstName() {
	}
	
	public FirstName(String firstName, String description, Boolean female, Boolean male) {
		this.firstName = firstName;
		this.description = description;
		this.female = female;
		this.male = male;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the female
	 */
	public Boolean getFemale() {
		return female;
	}

	/**
	 * @param female the female to set
	 */
	public void setFemale(Boolean female) {
		this.female = female;
	}

	/**
	 * @return the male
	 */
	public Boolean getMale() {
		return male;
	}

	/**
	 * @param male the male to set
	 */
	public void setMale(Boolean male) {
		this.male = male;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FirstName [firstName=" + firstName + ", description=" + description + ", female=" + female + ", male="
				+ male + "]";
	}
}
