package com.api.rest.rpg.characterfirstnames.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@IdClass(ComingFromId.class)
@Table(name = "coming_from")
@EntityListeners(AuditingEntityListener.class)
public class ComingFrom implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private String countryCode;
	
	@Id
	private Integer fkFirstNameId;

	public ComingFrom() {
	}

	public ComingFrom(String countryCode, Integer fkFirstNameId) {
		this.countryCode = countryCode;
		this.fkFirstNameId = fkFirstNameId;
	}

	/**
	 * @return the fkCountryId
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param fkCountryId the fkCountryId to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the fkFirstNameId
	 */
	public Integer getFkFirstNameId() {
		return fkFirstNameId;
	}

	/**
	 * @param fkFirstNameId the fkFirstNameId to set
	 */
	public void setFkFirstNameId(Integer fkFirstNameId) {
		this.fkFirstNameId = fkFirstNameId;
	}
}
