package com.api.rest.rpg.characterfirstnames.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.rest.rpg.characterfirstnames.model.FirstName;

@Repository
public interface FirstNameRepository extends JpaRepository<FirstName, Integer> {

}
