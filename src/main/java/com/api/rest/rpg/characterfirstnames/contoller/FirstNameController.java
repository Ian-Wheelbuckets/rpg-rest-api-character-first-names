package com.api.rest.rpg.characterfirstnames.contoller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.rest.rpg.characterfirstnames.dao.FirstNameRepository;
import com.api.rest.rpg.characterfirstnames.exception.UnreachableFirstNamesException;
import com.api.rest.rpg.characterfirstnames.model.FirstName;

@RestController
@RequestMapping("/api")
public class FirstNameController {
	
	@Autowired
	private FirstNameRepository firstNameRep;
	
	@RequestMapping(value="/first-names", method=RequestMethod.GET)
	public List<FirstName> getAllFirstNames() {
		List<FirstName> fns = firstNameRep.findAll();
		
		if(fns==null)
			throw new UnreachableFirstNamesException("The serveur is unable to reach the list of last names.");
		
		return fns;
	}
	
	@RequestMapping(value="/first-names/id/{id}", method=RequestMethod.GET)
	public Optional<FirstName> getFirstNameById(@PathVariable Integer id) {
		Optional<FirstName> fn = firstNameRep.findById(id);
		
		if(!fn.isPresent())
			throw new UnreachableFirstNamesException("The serveur is unable to reach the  last name.");
		
		return fn;
	}
}
