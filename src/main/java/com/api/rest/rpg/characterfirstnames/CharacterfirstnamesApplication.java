package com.api.rest.rpg.characterfirstnames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharacterfirstnamesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharacterfirstnamesApplication.class, args);
	}

}
