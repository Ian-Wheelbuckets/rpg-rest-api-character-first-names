package com.api.rest.rpg.characterfirstnames.model;

import java.io.Serializable;

public class ComingFromId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public String countryCode;
	
	public Integer fkFirstNameId;

	public ComingFromId() {
	}

	/**
	 * @return the fkCountryId
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param fkCountryId the fkCountryId to set
	 */
	public void setFkCountryId(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the fkFirstNameId
	 */
	public Integer getFkFirstNameId() {
		return fkFirstNameId;
	}

	/**
	 * @param fkFirstNameId the fkFirstNameId to set
	 */
	public void setFkFirstNameId(Integer fkFirstNameId) {
		this.fkFirstNameId = fkFirstNameId;
	}
}
